<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    //
    public function userStore(Request $request)
    {
        $product = User::create($request->all());
        return response()->json($product, 200);
    }
    public function update(Request $request, $id) {
        $product = User::find($id);
        $product->update($request->all());
        return response()->json($product,200); 
    }
    public function delete ($id) {
        $product = User::find($id);
        $product->delete();
    }
}
