<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\RegisterController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products', function() {
    return new ProductCollection(Product::all());
});

Route::get('products/{id}', function ($id) {
    return new ProductResource(Product::find($id));
});

Route::post('products', [ProductController::class, 'store']);
Route::put('product/{id}', [ProductController::class, 'update']);
Route::delete('products/delete/{id}', [ProductController::class, 'delete']);
Route::post('user', [RegisterController::class, 'userStore']);
Route::put('user/{id}', [RegisterController::class, 'update']);
Route::delete('user/delete/{id}', [RegisterController::class, 'delete']);